<?php

namespace App\Http\Controllers;
use App\Models\Leaves;
use Illuminate\Http\Request;
use App\Http\Resources\LeaveResources;
class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {   $param = request()->input('param');

 if ($param) {
            $employees=Leaves::where('reason','like','%'.$param.'%')->paginate(2);
        } else {
            $employees=Leaves::paginate(2);
        }

      if ( $employees->isEmpty()) {
            return response()->json(['message' => ' Blogs not found'], 404);
        } else {
            return LeaveResources::collection($employees); 
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        return Leaves::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return Leaves::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {

        $leaves=Leaves::findOrFail($id);
        $leaves->update($request->all());
        return $leaves;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $leaves=Leaves::findOrFail($id);
        $leaves->delete();
        return response()->json(['message' => 'Product deleted']);
    }
}
